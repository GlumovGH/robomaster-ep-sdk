import socket
import sys

host = "192.168.2.1"
port = 40923

address = (host, int(port))
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(address)

s.shutdown(socket.SHUT_WR)
s.close()
